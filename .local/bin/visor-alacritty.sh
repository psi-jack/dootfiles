#!/bin/sh

exec /usr/bin/alacritty --config-file ${XDG_CONFIG_HOME:-$HOME/.config}/alacritty/visor-dbus.toml -e "tmux" -2 a

