#!/bin/sh

exec /usr/bin/kitty -T KittyVisor --class "KittyVisor" -c ${XDG_CONFIG_HOME:-$HOME/.config}/kitty/visor.conf

