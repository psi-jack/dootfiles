# Via https://tanguy.ortolo.eu/blog/article25/shrc
#
# At startup, depending on the case:
# - run as a login shell (or with the option --login), it executes profile (or
# bash_profile instead if it exists (only user-specific version));
# - run as an interactive, non-login shell, it executes bashrc (the system-wide
# version is called bash.bashrc).
#
# At exit, it executes ~/.bash_logout (the system-wide version is called
# bash.bash_logout).
# Note the funny (read: insane) non-login condition for executing bashrc: it is
# often worked around by having the profile execute bashrc anyway.

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Source global definitions
test -r /etc/bashrc && . /etc/bashrc

test -r ~/.shell-env && . ~/.shell-env
test -r ~/.shell-aliases && . ~/.shell-aliases
test -r ~/.shell-common && . ~/.shell-common

PS1='[\u@\h \W]\$ '

test -r ~/.bashrc.local && . ~/.bashrc.local

if command -v fzf-share >/dev/null; then
  source "$(fzf-share)/key-bindings.bash"
  source "$(fzf-share)/completion.bash"
fi

## Load External Components

eval "$(direnv hook bash)"
eval "$(starship init bash)"
source ~/.local/share/blesh/ble.sh
eval "$(atuin init bash)"
#eval "$(keychain --eval --quiet --eval --systemd id_ed25519 id_rsa)"

